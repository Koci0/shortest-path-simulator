
# Shortest Path Simulator

This project aims to create a GUI simulator for various algorithm for finding the shortest path in a graph. Done for the "Computer Networks Integration" course at Cracow University of Technology.

## Build

Build using CMake:
```
cmake -S . -B build
cmake --build build
cd build
make
```

