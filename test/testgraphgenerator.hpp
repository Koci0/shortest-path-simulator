
#include <QtTest>

#include "Graph.h"
#include "structs.h"

#include <iostream>

class TestGraphGenerator: public QObject
{
    Q_OBJECT
private slots:

    void testRegularConstant10()
    {
        // Given
        int n_nodes = 10;
        int degree = 2;
        Graph graph(n_nodes, degree, Regularity::REGULAR, Weight::CONSTANT);

        // When
        graph.generate();

        // Then
        int less_degree_count = 0;
        for (auto &node : graph.getNodes()) {
            if (node->getEdges().size() < degree) {
                less_degree_count++;
            }
        }
        QVERIFY2(less_degree_count <= 2, "There are more than 1 nodes with not enough edges");

        int wrong_weight_count = 0;
        QVector<Edge *> wrong_edges;
        for (auto &node : graph.getNodes()) {
            for (auto &edge : node->getEdges()) {
                if (edge->getValue() != 1) {
                    if (not wrong_edges.contains(edge)) {
                        wrong_edges.append(edge);
                        wrong_weight_count++;
                    }
                }
            }
        }
        QVERIFY2(wrong_weight_count == 0, "There are edges with value different than 1");
    }

    void testRegularConstant100()
    {
        // Given
        int n_nodes = 100;
        int degree = 5;
        Graph graph(n_nodes, degree, Regularity::REGULAR, Weight::CONSTANT);

        // When
        graph.generate();

        // Then
        int less_degree_count = 0;
        for (auto &node : graph.getNodes()) {
            if (node->getEdges().size() < degree) {
                less_degree_count++;
            }
        }
        QVERIFY2(less_degree_count <= 2, "There are more than 1 nodes with not enough edges");

        int wrong_weight_count = 0;
        QVector<Edge *> wrong_edges;
        for (auto &node : graph.getNodes()) {
            for (auto &edge : node->getEdges()) {
                if (edge->getValue() != 1) {
                    if (not wrong_edges.contains(edge)) {
                        wrong_edges.append(edge);
                        wrong_weight_count++;
                    }
                }
            }
        }
        QVERIFY2(wrong_weight_count == 0, "There are edges with value different than 1");
    }

    void testRegularConstant1000()
    {
        // Given
        int n_nodes = 1000;
        int degree = 10;
        Graph graph(n_nodes, degree, Regularity::REGULAR, Weight::CONSTANT);

        // When
        graph.generate();

        // Then
        int less_degree_count = 0;
        for (auto &node : graph.getNodes()) {
            if (node->getEdges().size() < degree) {
                less_degree_count++;
            }
        }
        QVERIFY2(less_degree_count <= 2, "There are more than 1 nodes with not enough edges");

        int wrong_weight_count = 0;
        QVector<Edge *> wrong_edges;
        for (auto &node : graph.getNodes()) {
            for (auto &edge : node->getEdges()) {
                if (edge->getValue() != 1) {
                    if (not wrong_edges.contains(edge)) {
                        wrong_edges.append(edge);
                        wrong_weight_count++;
                    }
                }
            }
        }
        QVERIFY2(wrong_weight_count == 0, "There are edges with value different than 1");
    }

    void testRegularRandom10()
    {
        // Given
        int n_nodes = 10;
        int degree = 2;
        Graph graph(n_nodes, degree, Regularity::REGULAR, Weight::RANDOM);

        // When
        graph.generate();

        // Then
        int less_degree_count = 0;
        for (auto &node : graph.getNodes()) {
            if (node->getEdges().size() < degree) {
                less_degree_count++;
            }
        }
        QVERIFY2(less_degree_count <= 2, "There are more than 1 nodes with not enough edges");

        QSet<int> edges_values;
        for (auto &node : graph.getNodes()) {
            for (auto &edge : node->getEdges()) {
                edges_values.insert(edge->getValue());
            }
        }
        QVERIFY2(edges_values.size() >= 2, "There are no edges with random values");
    }

    void testRegularRandom100()
    {
        // Given
        int n_nodes = 100;
        int degree = 5;
        Graph graph(n_nodes, degree, Regularity::REGULAR, Weight::RANDOM);

        // When
        graph.generate();

        // Then
        int less_degree_count = 0;
        for (auto &node : graph.getNodes()) {
            if (node->getEdges().size() < degree) {
                less_degree_count++;
            }
        }

        QSet<int> edges_values;
        for (auto &node : graph.getNodes()) {
            for (auto &edge : node->getEdges()) {
                edges_values.insert(edge->getValue());
            }
        }

        QVERIFY2(less_degree_count <= 2, "There are more than 1 nodes with not enough edges");
        QVERIFY2(edges_values.size() >= 2, "There are no edges with random values");
    }

    void testRegularRandom1000()
    {
        // Given
        int n_nodes = 1000;
        int degree = 10;
        Graph graph(n_nodes, degree, Regularity::REGULAR, Weight::RANDOM);

        // When
        graph.generate();

        // Then
        int less_degree_count = 0;
        for (auto &node : graph.getNodes()) {
            if (node->getEdges().size() < degree) {
                less_degree_count++;
            }
        }

        QSet<int> edges_values;
        for (auto &node : graph.getNodes()) {
            for (auto &edge : node->getEdges()) {
                edges_values.insert(edge->getValue());
            }
        }

        QVERIFY2(less_degree_count <= 2, "There are more than 1 nodes with not enough edges");
        QVERIFY2(edges_values.size() >= 2, "There are no edges with random values");
    }
};
