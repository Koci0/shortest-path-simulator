
#include <QtTest>

#include "Graph.h"

class TestGraphLogic : public QObject
{
    Q_OBJECT
private slots:
    void init()
    {
        auto node1 = new Node(0);
        auto node2 = new Node(1);
        auto node3 = new Node(2);
        auto node4 = new Node(3);
        auto node5 = new Node(4);
        auto node6 = new Node(5);
        auto node7 = new Node(6);
        this->nodes.append({node1, node2, node3, node4, node5, node6, node7});

        new Edge(node1, node2, 1);
        new Edge(node1, node3, 1);
        new Edge(node2, node3, 1);
        new Edge(node2, node4, 9);
        new Edge(node2, node5, 1);
        new Edge(node4, node6, 9);
        new Edge(node5, node6, 1);
        new Edge(node5, node7, 9);
        new Edge(node6, node7, 1);

        this->graph = new Graph(this->nodes);
        this->graph->handleRunRealtime();
    }

    void testDijkstra()
    {
        auto result = this->graph->runDijkstra(0, 6);
        QString expected = "0 > 1 > 4 > 5 > 6 > ";
        QCOMPARE(result, expected);
    }

    void cleanup()
    {
        delete this->graph;
    }

private:
    Graph *graph;
    QVector<Node *> nodes;

};
