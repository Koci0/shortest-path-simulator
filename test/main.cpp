
#include <QtTest>

#include "testgraphgenerator.hpp"
#include "testgraphlogic.hpp"


int main(int argc, char *argv[])
{
    int status = 0;
    QTest::setMainSourcePath(__FILE__, QT_TESTCASE_BUILDDIR);
    {
        TestGraphGenerator tc;
        status |= QTest::qExec(&tc, argc, argv);
    }
    {
        TestGraphLogic tc;
        status |= QTest::qExec(&tc, argc, argv);
    }
    return status;
}
