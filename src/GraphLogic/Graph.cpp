#include "Graph.h"

#include <algorithm>

#include <QQueue>
#include <QWaitCondition>
#include <QRandomGenerator>
#include <QThread>

#include "Debug.h"

// TODO: move max value of edge to GUI
#define EDGE_MAX 100

Graph::Graph(int number_of_nodes, int degree, Regularity regularity, Weight weight)
    : number_of_nodes(number_of_nodes), degree(degree), regularity(regularity), weight(weight) {
    this->flowControlMutex.unlock();
    this->runRealtime = false;
    this->runWithDelay = false;
    this->runNextStep = false;
}

Graph::Graph(QVector<Node *> &nodes) : nodes(nodes) {}

Graph::~Graph() {
    this->nodes.clear();
}

void Graph::generate() {
    this->nodes.clear();

    QVector<int> remaining_node_ids;
    for (int i = 0; i < this->number_of_nodes; i++) {
        int number_of_edges = this->getNumberOfEdges();
        this->nodes.append(new Node(i, number_of_edges));
        remaining_node_ids.append(i);
    }

    // Go through every node in an order
    for (auto node_id = 0; node_id < this->nodes.size(); node_id++) {
        auto node = this->nodes[node_id];

        for (int n_edges = node->getEdges().size(); n_edges < node->maximum_number_of_edges; n_edges++) {
            std::random_device rd;
            std::mt19937 g(rd());
            std::shuffle(remaining_node_ids.begin(), remaining_node_ids.end(), g);
            int next_node_id = -1;
            for (auto picked_node_id : remaining_node_ids) {
                if (picked_node_id == node_id) {
                    continue;
                }

                auto picked_node = this->nodes[picked_node_id];
                if (not node->getNeighbors().contains(picked_node) &&
                        picked_node->getEdges().size() < picked_node->maximum_number_of_edges) {
                    next_node_id = picked_node_id;
                    break;
                }
            }

            if (next_node_id == -1) {
                continue;
            }
            auto next_node = this->nodes[next_node_id];

            // Pick edge value
            unsigned int edge_value = 1;
            if (this->weight == Weight::CONSTANT) {
                edge_value = 1;
            } else { // Weight::RANDOM
                edge_value = QRandomGenerator::global()->bounded(1, EDGE_MAX);
            }

            // Create and add edge
            new Edge(node, next_node, edge_value); // Edge constructor automatically adds edge to both nodes

            node->addNeighbor(next_node);
            next_node->addNeighbor(node);

            if (node->getEdges().size() == node->maximum_number_of_edges) {
                for (int i = 0; i < remaining_node_ids.size(); i++) {
                    if (remaining_node_ids[i] == node->id) {
                        remaining_node_ids.remove(i);
                        break;
                    }
                }
            }
            if (next_node->getEdges().size() == next_node->maximum_number_of_edges) {
                for (int i = 0; i < remaining_node_ids.size(); i++) {
                    if (remaining_node_ids[i] == next_node->id) {
                        remaining_node_ids.remove(i);
                        break;
                    }
                }
            }
        }

    }

    this->print();
}

QString Graph::runDijkstra(int start_node_id, int destination_node_id) {
    Logger::info(TAG, "Running Dijkstra");

    // current node id -> (distance, previous node id)
    QMap<int, int> distance;
    QMap<int, int> previous;

    for (Node *node : this->nodes) {
        distance.insert(node->id, -1);
        previous.insert(node->id, -1);
    }

    int begin_node_id = start_node_id;
    distance[begin_node_id] = 0;
    previous[begin_node_id] = -1;
    int end_node_id = destination_node_id;

    QQueue<Node *> node_queue;
    node_queue.enqueue(this->nodes[begin_node_id]);

    this->printDistanceTable(distance, previous);

    QSet<Edge *> checked_edges;
    QString line = "";
    while (not node_queue.empty()) {
        Node *current_node = node_queue.dequeue();
        current_node->color = Qt::blue;
        emit graphUpdated();
        line = "Selected node: " + QString::number(current_node->id);
        Logger::info(TAG, line);
        emit textUpdated(line);
        this->waitForInput();

        for (Edge *adjacent_edge : current_node->getEdges()) {
            if (checked_edges.contains(adjacent_edge)) {
                continue;
            }
            checked_edges.insert(adjacent_edge);

            Node *adjacent_node = nullptr;
            if (adjacent_edge->getDestinationNode() != current_node) {
                adjacent_node = adjacent_edge->getDestinationNode();
            }
            else {
                adjacent_node = adjacent_edge->getSourceNode();
            }
            node_queue.enqueue(adjacent_node);
            adjacent_edge->color = Qt::blue;
            adjacent_node->color = Qt::blue;
            emit graphUpdated();
            line = "Checking edge: (" + QString::number(adjacent_edge->getSourceNode()->id) + " - " + QString::number(adjacent_edge->getDestinationNode()->id) + ")";
            Logger::info(TAG, line);
            emit textUpdated(line);

            int tmp_distance = distance[current_node->id] + adjacent_edge->getValue();
            if (previous[adjacent_node->id] == -1 || tmp_distance < distance[adjacent_node->id]) {
                distance[adjacent_node->id] = tmp_distance;
                previous[adjacent_node->id] = current_node->id;
            }
            this->waitForInput();
        }
        this->printDistanceTable(distance, previous);
    }

    // Get path elements
    bool path_found = false;
    QString path = "";
    QVector<Node *> path_nodes;
    QVector<Edge *> path_edges;
    int next_id = end_node_id;
    while (next_id != -1) {
        auto next_node = this->nodes[next_id];
        path_nodes.append(next_node);
        for (auto edge : next_node->getEdges()) {
            if ((edge->getSourceNode()->id == next_id && edge->getDestinationNode()->id == previous[next_id]) ||
                    (edge->getDestinationNode()->id == next_id && edge->getSourceNode()->id == previous[next_id])) {
                path_edges.append(edge);
                break;
            }
        }
        path = QString::number(next_id) + " > " + path;
        next_id = previous[next_id];

        if (next_id == begin_node_id) {
            path_found = true;
        }
    }

    // Color path elements
    if (path_found) {
        for (auto node : path_nodes) {
            node->color = Qt::green;
        }
        for (auto edge : path_edges) {
            edge->color = Qt::green;
        }
        QString line = "Path is " + path;
        Logger::info(TAG, line);
        emit textUpdated(line);
    }
    else {
        for (auto node : path_nodes) {
            node->color = Qt::red;
        }
        for (auto edge : path_edges) {
            edge->color = Qt::red;
        }
        Logger::info(TAG, "Could not find a path from " + QString::number(begin_node_id) + " to " + QString::number(end_node_id));
    }

    return path;
}

QVector<Node *> Graph::getNodes() {
    return this->nodes;
}

void Graph::print() {
    QString line = "\n";

    for (auto node : this->nodes) {
        line += QString::number(node->id) + ": ";
        for (auto edge : node->getEdges()) {
            line += "(" + QString::number(edge->getSourceNode()->id) + "," + QString::number(edge->getDestinationNode()->id) + ")[" + QString::number(edge->getValue()) + "]";
            line += "\t";
        }

        line += "\n";
    }

    Logger::info(TAG, line);
    emit textUpdated(line);
}

void Graph::printDistanceTable(QMap<int, int> &distance, QMap<int, int> &previous) {
    QString line = "Distance table\nCurrent node\tDistance\tPrevious node\n";
    for (auto row : distance.keys()) {
        line += QString::number(row) + "\t\t" + QString::number(distance[row]) + "\t\t" + QString::number(previous[row]) + "\n";
    }

    Logger::info(TAG, line);
    emit distanceTableUpdated(line);
}

int Graph::getNumberOfEdges()
{
    if (this->regularity == Regularity::REGULAR) {
        return degree;
    } else { // Regularity::IRREGULAR
        return QRandomGenerator::global()->bounded(1, degree + 1);
    }
}

bool Graph::waitForInput() {
    this->flowControlMutex.lock();
    if (not this->runRealtime) {
        this->flowControlMutex.unlock();
        while (true) {
            this->flowControlMutex.lock();
            if (this->runNextStep) {
                this->runNextStep = false;
                this->flowControlMutex.unlock();
                return true;
            }
            if (this->runWithDelay) {
                this->flowControlMutex.unlock();
                QThread::sleep(1);
                return true;
            }
            this->flowControlMutex.unlock();
        }
    }
    this->flowControlMutex.unlock();
    return true;
}

void Graph::handleRunRealtime() {
    this->runRealtime = true;
}

void Graph::handleRunStep() {
    this->flowControlMutex.lock();
    this->runWithDelay = true;
    this->flowControlMutex.unlock();
}

void Graph::handleNextStep() {
    this->flowControlMutex.lock();
    this->runNextStep = true;
    this->flowControlMutex.unlock();
}

void Graph::handlePauseStep() {
    this->flowControlMutex.lock();
    this->runWithDelay = false;
    this->flowControlMutex.unlock();
}
