#ifndef EDGE_H
#define EDGE_H

#include <QGraphicsItem>

class Node;

class Edge : public QGraphicsItem {
    const char *TAG = "Edge";

public:
    Edge(Node *sourceNode, Node *destinationNode, int value);
    Edge(Node *sourceNode, Node *destinationNode);

    Node *getSourceNode();
    Node *getDestinationNode();
    int getValue();
private:
    Node *sourceNode;
    Node *destinationNode;
    int value;

    // QGraphicsItem methods
public:
    void adjust();

    Qt::GlobalColor color;
protected:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
private:
    QPointF sourcePoint;
    QPointF destinationPoint;
};

#endif // EDGE_H
