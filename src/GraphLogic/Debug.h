#ifndef DEBUG_H
#define DEBUG_H

#include <QDebug>
#include <iostream>

class Logger {
  public:
    static void info(const char *tag, const char *msg) { qDebug() << "[" << tag << "] " << msg; }

    static void info(const char *tag, const QString &msg) {
        std::string std_msg = msg.toStdString();
        info(tag, std_msg.c_str());
    }

    static void error(const char *tag, const char *msg) { std::cerr << "[" << tag << "] " << msg << std::endl; }

    static void error(const char *tag, const QString &msg) {
        std::string std_msg = msg.toStdString();
        error(tag, std_msg.c_str());
    }
};

#endif // DEBUG_H
