#ifndef SIMULATION_H
#define SIMULATION_H

#include <QSharedPointer>
#include <QThread>

#include "structs.h"

class Graph;

class SimulationThread : public QThread {
    Q_OBJECT;
    const char *TAG = "SimulationThread";

  public:
    SimulationThread(QObject *parent, QSharedPointer<Graph> graph, Algorithm algorithm, int start_node_id, int destination_node_id);
    ~SimulationThread();

  signals:
    void resultReady(const QString &s);

  private:
    QSharedPointer<Graph> graph;
    Algorithm algorithm;
    int start_node_id;
    int destination_node_id;

    void run() override;
};

#endif // SIMULATION_H
