#include "Edge.h"

#include "Debug.h"
#include "Node.h"

#include <QtMath>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

Edge::Edge(Node *sourceNode, Node *destinationNode, int value) : sourceNode(sourceNode), destinationNode(destinationNode), value(value) {
    setAcceptedMouseButtons(Qt::NoButton);
    sourceNode->addEdge(this);
    destinationNode->addEdge(this);
    adjust();

    color = Qt::black;
}

Edge::Edge(Node *sourceNode, Node *destinationNode) : Edge(sourceNode, destinationNode, 0) {}

Node *Edge::getSourceNode() { return this->sourceNode; }

Node *Edge::getDestinationNode() { return this->destinationNode; }

int Edge::getValue() { return this->value; }

void Edge::adjust() {
    if (not this->sourceNode || not this->destinationNode) {
        Logger::error(TAG, "Found empty source or destination node in ::adjust");
        return;
    }

    QLineF line(mapFromItem(this->sourceNode, 0, 0), mapFromItem(this->destinationNode, 0, 0));
    qreal length = line.length();

    prepareGeometryChange();

    if (length > qreal(20.0)) {
        QPointF edgeOffset((line.dx() * 10) / length, (line.dy() * 10) / length);
        this->sourcePoint = line.p1() + edgeOffset;
        this->destinationPoint = line.p2() - edgeOffset;
    } else {
        this->sourcePoint = line.p1();
        this->destinationPoint = line.p2();
    }
}

QRectF Edge::boundingRect() const {
    if (not this->sourceNode || not this->destinationNode) {
        return QRectF();
    }

    qreal penWidth = 1;

    return QRectF(this->sourcePoint, QSizeF(this->destinationPoint.x() - this->sourcePoint.x(),
                                            this->destinationPoint.y() - this->sourcePoint.y()))
            .normalized();
}

void Edge::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    if (not this->sourceNode || not this->destinationNode) {
        Logger::error(TAG, "Found empty source or destination node in ::paint");
        return;
    }

    QLineF line(this->sourcePoint, this->destinationPoint);
    if (qFuzzyCompare(line.length(), qreal(0.0))) {
        return;
    }

    painter->setPen(QPen(this->color, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter->drawLine(line);

    QFont font = painter->font();
    font.setPixelSize(24);
    painter->setFont(font);
    painter->setPen(this->color);
    painter->drawText((this->destinationPoint.x() + this->sourcePoint.x()) / qreal(2.0),
                      (this->destinationPoint.y() + this->sourcePoint.y()) / qreal(2.0), QString::number(this->value));

}
