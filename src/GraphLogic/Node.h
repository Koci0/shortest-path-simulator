#ifndef NODE_H
#define NODE_H

#include <QSet>
#include <QVector>
#include <QGraphicsItem>

#include "Debug.h"

class Edge;

class Node : public QGraphicsItem {
    const char *TAG = "Node";

public:
    Node(int id, int maximum_number_of_edges);
    Node(int id, QVector<Edge *> &edges);
    Node(int id);
    ~Node();

    QSet<Edge *> &getEdges();
    QSet<Node *> &getNeighbors();

    void addEdge(Edge *edge);
    void addNeighbor(Node *neighbor);

    int id;
    int maximum_number_of_edges;

private:
    QSet<Edge *> edges;
    QSet<Node *> neighbors;

    // QGraphicsItem methods
public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    QPainterPath shape() const override;

    Qt::GlobalColor color;
protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
private:
    int radius = 40;
};

#endif // NODE_H
