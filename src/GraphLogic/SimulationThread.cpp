#include "SimulationThread.h"

#include "Graph.h"

void SimulationThread::run() {
    QString result = "";

    if (this->algorithm == Algorithm::DIJKSTRA) {
        result = this->graph->runDijkstra(this->start_node_id, this->destination_node_id);
    } else {
        Logger::error(TAG, "DIJKSTRA not selected which is an error");
        abort();
    }

    emit resultReady(result);
}

SimulationThread::SimulationThread(QObject *parent, QSharedPointer<Graph> graph, Algorithm algorithm, int start_node_id, int destination_node_id) : QThread(parent), graph(graph), algorithm(algorithm), start_node_id(start_node_id), destination_node_id(destination_node_id) {
}

SimulationThread::~SimulationThread() {}
