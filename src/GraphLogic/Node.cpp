#include "Node.h"

#include <QString>

#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include "Edge.h"

Node::Node(int id, int maximum_number_of_edges) : id(id), maximum_number_of_edges(maximum_number_of_edges) {
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setZValue(-1);

    color = Qt::black;
}

Node::Node(int id, QVector<Edge *> &edges) : id(id) {
    for (auto &edge : edges) {
        this->addEdge(edge);
    }
}

Node::Node(int id) : Node(id, 0) {}

Node::~Node() {
    this->edges.clear();
}

QSet<Edge *> &Node::getEdges() { return this->edges; }

QSet<Node *> &Node::getNeighbors() { return this->neighbors; }

void Node::addEdge(Edge *edge) {
    this->edges.insert(edge);
}

void Node::addNeighbor(Node *neighbor) { this->neighbors.insert(neighbor); }

QRectF Node::boundingRect() const {
    qreal adjust = 2;
    return QRectF(-(this->radius / 2.0) - adjust, -(this->radius / 2.0) - adjust, this->radius + 3 + adjust, this->radius + 3 + adjust);
}

QPainterPath Node::shape() const {
    QPainterPath path;
    path.addEllipse(-(this->radius / 2.0), -(this->radius / 2.0), this->radius, this->radius);
    return path;
}

void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *) {
    painter->setPen(QPen(this->color, 0));
    painter->drawEllipse(-(this->radius / 2.0), -(this->radius / 2.0), this->radius, this->radius);

    QFont font = painter->font();
    font.setPixelSize(24);
    painter->setFont(font);
    painter->setPen(this->color);
    painter->drawText(-10, -10, 20, 20, Qt::AlignCenter, QString::number(this->id), nullptr);
}

QVariant Node::itemChange(GraphicsItemChange change, const QVariant &value) {
    switch (change) {
    case ItemPositionHasChanged:
        for (auto edge : this->edges) {
            edge->adjust();
        }
        break;
    default:
        break;
    }

    return QGraphicsItem::itemChange(change, value);
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    update();
    QGraphicsItem::mousePressEvent(event);
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}
