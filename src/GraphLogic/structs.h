#ifndef STRUCTS_H
#define STRUCTS_H

enum class Regularity { REGULAR, IRREGULAR };

enum class Weight { CONSTANT, RANDOM };

enum class Algorithm { DIJKSTRA };

enum class SimulationType { REALTIME, STEP_BY_STEP };

#endif // STRUCTS_H
