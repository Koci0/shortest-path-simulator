#ifndef GRAPH_H
#define GRAPH_H

#include <QVector>
#include <QMutex>

#include "Edge.h"
#include "Node.h"
#include "structs.h"

enum { REGULAR, IRREGULAR };

class Graph : public QObject {
    Q_OBJECT;

    const char *TAG = "Graph";

    QVector<Node *> nodes;

  public:
    Graph(int number_of_nodes, int degree, Regularity regularity, Weight weigth);
    Graph(QVector<Node *> &nodes);
    ~Graph();

    void generate();
    QString runDijkstra(int start_node_id, int destination_node_id);

    QVector<Node *> getNodes();

 signals:
    void graphUpdated();
    void textUpdated(QString text);
    void distanceTableUpdated(QString text);

  public slots:
    void handleRunRealtime();
    void handleRunStep();
    void handleNextStep();
    void handlePauseStep();

  private:
    void print();
    void printDistanceTable(QMap<int, int> &distance, QMap<int, int> &previous);
    int getNumberOfEdges();
    bool waitForInput();

    int number_of_nodes;
    int degree;
    Regularity regularity;
    Weight weight;

    QMutex flowControlMutex;
    bool runRealtime;
    bool runWithDelay;
    bool runNextStep;
};

#endif // GRAPH_H
