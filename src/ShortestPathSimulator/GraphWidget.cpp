#include "GraphWidget.h"

#include <QGraphicsTextItem>
#include <QtMath>
#include <QWheelEvent>

#include "Graph.h"
#include "Debug.h"

GraphWidget::GraphWidget(QWidget *parent) : QGraphicsView(parent)
{
    QGraphicsScene *scene = new QGraphicsScene(this);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene->setSceneRect(-200, -200, 400, 400); // TODO: why those dimensions?
    setScene(scene);
    setCacheMode(CacheBackground);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    scale(qreal(0.8), qreal(0.8));
}

void GraphWidget::setGraph(Graph *graph) {
    this->graph = graph;
    this->setupGraph();
}

void GraphWidget::setupGraph() {
    auto scene = this->scene();
    scene->clear();

    auto nodes = this->graph->getNodes();
    auto nodes_size = nodes.size();

    float radius = qMax(scene->sceneRect().height(), scene->sceneRect().width()) * 0.75;
    float circle_part = 360 / nodes_size;

    QSet<Edge *> edges;
    for (auto node : nodes) {
        scene->addItem(node);

        auto radians = qDegreesToRadians((node->id + 1) * circle_part);
        int x = radius * qCos(radians);
        int y = radius * qSin(radians);

        node->setPos(x, y);
        for (auto edge : node->getEdges()) {
            edges.insert(edge);
        }
    }

    for (auto edge : edges) {
        scene->addItem(edge);
        edge->adjust();
    }
}

void GraphWidget::updateGraph() {
    for (auto item : this->scene()->items()) {
        item->update(item->boundingRect());
    }
}

void GraphWidget::wheelEvent(QWheelEvent *event) {
    scaleView(pow(2.0, event->angleDelta().y() / 240.0));
}

void GraphWidget::scaleView(qreal scaleFactor) {
    qreal factor = transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.07 || factor > 100) {
        return;
    }

    scale(scaleFactor, scaleFactor);
}
