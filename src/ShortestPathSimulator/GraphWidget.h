#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include <QGraphicsView>

class Graph;

class GraphWidget : public QGraphicsView
{
    Q_OBJECT
    const char *TAG = "GraphWidget";
public:
    GraphWidget(QWidget *parent);
    void setGraph(Graph *graph);
    void setupGraph();

public slots:
    void updateGraph();
protected:
    void wheelEvent(QWheelEvent *event) override;
    void scaleView(qreal scaleFactor);
private:
    Graph *graph;
};

#endif // GRAPHWIDGET_H
