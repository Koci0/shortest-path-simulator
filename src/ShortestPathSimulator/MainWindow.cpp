#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QButtonGroup>
#include <QSharedPointer>
#include <QScrollBar>

#include "Debug.h"
#include "Graph.h"
#include "GraphWidget.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    this->graph_widget = new GraphWidget(this);

    ui->right_verticalLayout->addWidget(graph_widget);
    auto horizontalLayout = new QHBoxLayout(this);
    ui->right_verticalLayout->addLayout(horizontalLayout);

    this->text_edit = new QTextEdit(this);
    text_edit->setReadOnly(true);
    horizontalLayout->addWidget(text_edit);

    this->distanceTable_textEdit = new QTextEdit(this);
    this->distanceTable_textEdit->setReadOnly(true);
    horizontalLayout->addWidget(distanceTable_textEdit);

    this->start_node_id = 0;
    this->destination_node_id = 0;
    this->number_of_nodes = ui->nodes_spinBox->value();
    // TODO: limit degree so it is not higher than number of nodes
    this->degree = ui->degree_spinBox->value();
    this->checkRegularity();
    this->checkWeight();
    this->algorithm = Algorithm::DIJKSTRA;
}

MainWindow::~MainWindow() {
    delete ui;
    delete graph_widget;
}

void MainWindow::updateText(QString text) {
    Logger::info(TAG, text);
    this->text_edit->setPlainText(this->text_edit->toPlainText() + text + "\n");
    this->text_edit->verticalScrollBar()->setValue(this->text_edit->verticalScrollBar()->maximum());
}

void MainWindow::updateDistanceTable(QString text) {
    Logger::info(TAG, text);
    this->distanceTable_textEdit->setPlainText(text);
    this->distanceTable_textEdit->verticalScrollBar()->setValue(this->distanceTable_textEdit->verticalScrollBar()->maximum());
}

void MainWindow::on_startRealtime_pushButton_clicked() {
    emit runRealtime();
    ui->startRealtime_pushButton->setDisabled(true);
    this->startSimulation();
    ui->stopRealtime_pushButton->setEnabled(true);
}

void MainWindow::on_stopRealtime_pushButton_clicked() {
    ui->stopRealtime_pushButton->setDisabled(true);
    this->stopSimulation();
    ui->startRealtime_pushButton->setEnabled(true);
}

void MainWindow::handleSimulationFinished(const QString &result) {
    Logger::info(TAG, "Simulation finished with result: " + result);
    this->checkSimulationType();
    delete this->simulationThread;
    this->simulationThread = nullptr;

    disconnect(this->graph.get(), &Graph::graphUpdated, this->graph_widget, &GraphWidget::updateGraph);
    disconnect(this->graph.get(), &Graph::textUpdated, this, &MainWindow::updateText);
    disconnect(this->graph.get(), &Graph::distanceTableUpdated, this, &MainWindow::updateDistanceTable);
}

void MainWindow::startSimulation() {
    this->text_edit->document()->setPlainText("");

    if (this->simulationThread != nullptr) {
        this->simulationThread->exit();
        delete this->simulationThread;
    }
    SimulationThread *simulation = new SimulationThread(this, this->graph, this->algorithm, this->start_node_id, this->destination_node_id);
    this->simulationThread = simulation;

    for (auto node : this->graph->getNodes()) {
        node->color = Qt::black;
        for (auto edge : node->getEdges()) {
            edge->color = Qt::black;
        }
    }
    connect(this->graph.get(), &Graph::graphUpdated, this->graph_widget, &GraphWidget::updateGraph);
    connect(this->graph.get(), &Graph::textUpdated, this, &MainWindow::updateText);
    connect(this->graph.get(), &Graph::distanceTableUpdated, this, &MainWindow::updateDistanceTable);

    connect(simulation, &SimulationThread::resultReady, this, &MainWindow::handleSimulationFinished);
    connect(simulation, &SimulationThread::finished, simulation, &QObject::deleteLater);

    simulation->start();
}

void MainWindow::stopSimulation() {
    if (this->simulationThread != nullptr && this->simulationThread->isRunning()) {
        this->simulationThread->terminate();
        Logger::info(TAG, "Simulation terminated");
    } else {
        Logger::info(TAG, "Trying to stop nullptr simulation thread. Abort!");
        abort();
    }
}

void MainWindow::on_regular_radioButton_toggled(bool checked) {
    if (not checked) {
        return;
    } else {
        this->checkRegularity();
    }
}

void MainWindow::on_irregular_radioButton_toggled(bool checked) {
    if (not checked) {
        return;
    } else {
        this->checkRegularity();
    }
}

void MainWindow::checkRegularity() {
    if (ui->regular_radioButton->isChecked()) {
        this->regularity = Regularity::REGULAR;
    } else if (ui->irregular_radioButton->isChecked()) {
        this->regularity = Regularity::IRREGULAR;
    } else {
        Logger::error(TAG, "Neither Regular nor Irregular radioButton is checked. Abort!");
        abort();
    }

    QString msg = "Regularity is ";
    msg += (this->regularity == Regularity::REGULAR ? "REGULAR" : "IRREGULAR");
    Logger::info(TAG, msg);
}

void MainWindow::on_constantWeight_radioButton_toggled(bool checked) {
    if (not checked) {
        return;
    } else {
        this->checkWeight();
    }
}

void MainWindow::on_randomWeight_radioButton_toggled(bool checked) {
    if (not checked) {
        return;
    } else {
        this->checkWeight();
    }
}

void MainWindow::checkWeight() {
    if (ui->constantWeight_radioButton->isChecked()) {
        this->weight = Weight::CONSTANT;
    } else if (ui->randomWeight_radioButton->isChecked()) {
        this->weight = Weight::RANDOM;
    } else {
        Logger::error(TAG, "Neither Constant nor Random weight radioButton is checked. Abort!");
        abort();
    }

    QString msg = "Weight is ";
    msg += (this->weight == Weight::CONSTANT ? "CONSTANT" : "RANDOM");
    Logger::info(TAG, msg);
}

void MainWindow::on_realtime_radioButton_toggled(bool checked) {
    if (not checked) {
        return;
    } else {
        this->checkSimulationType();
    }
}

void MainWindow::on_step_radioButton_toggled(bool checked) {
    if (not checked) {
        return;
    } else {
        this->checkSimulationType();
    }
}

void MainWindow::checkSimulationType() {
    if (ui->realtime_radioButton->isChecked()) {
        this->simulation_type = SimulationType::REALTIME;

        ui->startRealtime_pushButton->setEnabled(true);
        ui->stopRealtime_pushButton->setDisabled(true);

        ui->runStep_pushButton->setDisabled(true);
        ui->nextStep_pushButton->setDisabled(true);
        ui->pauseStep_pushButton->setDisabled(true);
    } else if (ui->step_radioButton->isChecked()) {
        this->simulation_type = SimulationType::STEP_BY_STEP;

        ui->startRealtime_pushButton->setDisabled(true);
        ui->stopRealtime_pushButton->setDisabled(true);

        ui->runStep_pushButton->setEnabled(true);
        ui->nextStep_pushButton->setEnabled(true);
        ui->pauseStep_pushButton->setDisabled(true);
    } else {
        Logger::error(TAG, "Neither realtime nor step-by-step simulation radioButton is checked. Abort!");
        abort();
    }

    QString msg = "Simulation type is ";
    msg += (this->simulation_type == SimulationType::REALTIME ? "realtime" : "step-by-step");
    Logger::info(TAG, msg);
}

void MainWindow::on_nodes_spinBox_valueChanged(int arg1) { this->number_of_nodes = arg1; }

void MainWindow::on_degree_spinBox_valueChanged(int arg1) {
    if (arg1 <= this->number_of_nodes) {
        this->degree = arg1;
    }
    else {
        ui->degree_spinBox->setValue(this->degree);
    }
}

void MainWindow::on_generateGraph_pushButton_clicked()
{
    this->graph = QSharedPointer<Graph>::create(this->number_of_nodes, this->degree, this->regularity, this->weight);
    this->graph->generate();

    this->graph_widget->setGraph(this->graph.get());

    this->checkSimulationType();

    ui->startNode_spinBox->setValue(0);
    ui->destinationNode_spinBox->setValue(this->number_of_nodes - 1);

    connect(this, &MainWindow::runRealtime, this->graph.get(), &Graph::handleRunRealtime);
    connect(this, &MainWindow::runStep, this->graph.get(), &Graph::handleRunStep);
    connect(this, &MainWindow::nextStep, this->graph.get(), &Graph::handleNextStep);
    connect(this, &MainWindow::pauseStep, this->graph.get(), &Graph::handlePauseStep);
}

void MainWindow::on_runStep_pushButton_clicked()
{
    if (this->simulationThread == nullptr || this->simulationThread->isFinished()) {
        this->startSimulation();
    }
    ui->runStep_pushButton->setDisabled(true);
    ui->pauseStep_pushButton->setEnabled(true);

    emit runStep();
}

void MainWindow::on_nextStep_pushButton_clicked()
{
    if (this->simulationThread == nullptr) {
        this->startSimulation();
    }
    else {
        emit nextStep();
    }
}

void MainWindow::on_pauseStep_pushButton_clicked()
{
    ui->runStep_pushButton->setEnabled(true);
    ui->pauseStep_pushButton->setDisabled(true);

    emit pauseStep();
}

void MainWindow::on_startNode_spinBox_valueChanged(int arg1)
{
    if (arg1 >= 0 && arg1 <= this->number_of_nodes - 2) {
        this->start_node_id = arg1;
    }
    else {
        ui->startNode_spinBox->setValue(this->start_node_id);
    }
}


void MainWindow::on_destinationNode_spinBox_valueChanged(int arg1)
{
    if (arg1 >= 0 && arg1 <= this->number_of_nodes - 1 && arg1 > this->start_node_id) {
        this->destination_node_id = arg1;
    }
    else {
        ui->destinationNode_spinBox->setValue(this->destination_node_id);
    }
}

