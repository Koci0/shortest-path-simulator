#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>
#include <QMainWindow>
#include <QThread>
#include <QTextEdit>

#include "SimulationThread.h"
#include "structs.h"

class GraphWidget;

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT
    const char *TAG = "MainWindow";

  public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void runRealtime();
    void runStep();
    void nextStep();
    void pauseStep();

public slots:
    void updateText(QString text);
    void updateDistanceTable(QString text);

  private slots:
    // Custom
    void handleSimulationFinished(const QString &result);

    // Qt-defined
    void on_startRealtime_pushButton_clicked();
    void on_stopRealtime_pushButton_clicked();

    void on_regular_radioButton_toggled(bool checked);
    void on_irregular_radioButton_toggled(bool checked);

    void on_constantWeight_radioButton_toggled(bool checked);
    void on_randomWeight_radioButton_toggled(bool checked);

    void on_realtime_radioButton_toggled(bool checked);
    void on_step_radioButton_toggled(bool checked);

    void on_nodes_spinBox_valueChanged(int arg1);
    void on_degree_spinBox_valueChanged(int arg1);

    void on_generateGraph_pushButton_clicked();

    void on_runStep_pushButton_clicked();

    void on_pauseStep_pushButton_clicked();

    void on_nextStep_pushButton_clicked();

    void on_startNode_spinBox_valueChanged(int arg1);

    void on_destinationNode_spinBox_valueChanged(int arg1);

private:
    Ui::MainWindow *ui;
    GraphWidget *graph_widget;
    QTextEdit *text_edit;
    QTextEdit *distanceTable_textEdit;
    QThread *simulationThread = nullptr;

    int start_node_id;
    int destination_node_id;
    unsigned int number_of_nodes;
    unsigned int degree;
    Regularity regularity;
    Weight weight;
    Algorithm algorithm;
    SimulationType simulation_type;

    QSharedPointer<Graph> graph;

    void startSimulation();
    void stopSimulation();
    void checkRegularity();
    void checkWeight();
    void checkSimulationType();
};
#endif // MAINWINDOW_H
